upstream gestiocof {
  # Daphne listens on a unix socket
  server unix:/srv/gestiocof/gestiocof.sock;
}

server {
  listen 80;
  listen [::]:80;

  server_name _;
  root /srv/gestiocof/;

  # Redirection:
  rewrite ^/$        http://localhost:8080/gestion/ redirect;
  rewrite ^/gestion$ http://localhost:8080/gestion/ redirect;

  # Les pages statiques sont servies à part.
  location /gestion/static { try_files $uri $uri/ =404; }
  location /gestion/media  { try_files $uri $uri/ =404; }

  # On proxy-pass les requêtes vers les pages dynamiques à daphne
  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-SSL-Client-Serial $ssl_client_serial;
    proxy_set_header X-SSL-Client-Verify $ssl_client_verify;
    proxy_set_header X-SSL-Client-S-DN   $ssl_client_s_dn;
    proxy_pass http://gestiocof;
  }

  # Pour les websockets :
  # See http://nginx.org/en/docs/http/websocket.html.
  location /ws/ {
    proxy_buffering off;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_pass http://gestiocof/ws/;
  }
}
