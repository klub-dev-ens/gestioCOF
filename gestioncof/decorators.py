import logging
from functools import wraps

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render

logger = logging.getLogger(__name__)


def cof_required(view_func):
    """Décorateur qui vérifie que l'utilisateur est connecté et membre du COF.

    - Si l'utilisteur n'est pas connecté, il est redirigé vers la page de
      connexion
    - Si l'utilisateur est connecté mais pas membre du COF, il obtient une
      page d'erreur lui demandant de s'inscrire au COF
    """

    def is_cof(user):
        try:
            return user.profile.is_cof
        except AttributeError:
            return False

    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if is_cof(request.user):
            return view_func(request, *args, **kwargs)

        return render(request, "cof-denied.html", status=403)

    return login_required(_wrapped_view)


def buro_required(view_func):
    """Décorateur qui vérifie que l'utilisateur est connecté et membre du burô.

    - Si l'utilisateur n'est pas connecté, il est redirigé vers la page de
      connexion
    - Si l'utilisateur est connecté mais pas membre du burô, il obtient une
      page d'erreur 403 Forbidden
    """

    def is_buro(user):
        try:
            return user.profile.is_buro
        except AttributeError:
            return False

    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if is_buro(request.user):
            return view_func(request, *args, **kwargs)

        return render(request, "buro-denied.html", status=403)

    return login_required(_wrapped_view)


class CofRequiredMixin(PermissionRequiredMixin):
    def has_permission(self):
        if not self.request.user.is_authenticated:
            return False
        try:
            return self.request.user.profile.is_cof
        except AttributeError:
            return False


class BuroRequiredMixin(PermissionRequiredMixin):
    def has_permission(self):
        if not self.request.user.is_authenticated:
            return False
        try:
            return self.request.user.profile.is_buro
        except AttributeError:
            logger.error(
                "L'utilisateur %s n'a pas de profil !", self.request.user.username
            )
            return False
