from django.contrib.auth import views as django_auth_views
from django.urls import include, path
from django.views.generic.base import TemplateView
from django_cas_ng import views as django_cas_views

from gestioncof import csv_views, views

export_patterns = [
    path("members", views.export_members, name="export.members"),
    path(
        "mega/avecremarques",
        views.export_mega_remarksonly,
        name="export.mega.remarks",
    ),
    path(
        "mega/participants",
        views.export_mega_participants,
        name="export.mega.participants",
    ),
    path("mega/orgas", views.export_mega_orgas, name="export.mega.orgas"),
    path("mega/all", views.export_mega, name="export.mega.all"),
]

surveys_patterns = [
    path("<int:survey_id>/status", views.survey_status, name="survey.details.status"),
    path("<int:survey_id>", views.survey, name="survey.details"),
]

events_patterns = [
    path("<int:event_id>", views.event, name="event.details"),
    path("<int:event_id>/status", views.event_status, name="event.details.status"),
]

calendar_patterns = [
    path("subscription", views.calendar, name="calendar"),
    path("<slug:token>/calendar.ics", views.calendar_ics, name="calendar.ics"),
]

clubs_patterns = [
    path("membres/<slug:name>", views.membres_club, name="membres-club"),
    path("liste", views.liste_clubs, name="liste-clubs"),
    path(
        "change_respo/<slug:club_name>/<int:user_id>",
        views.change_respo,
        name="change-respo",
    ),
]

registration_patterns = [
    # Inscription d'un nouveau membre
    path("", views.registration, name="registration"),
    path(
        "clipper/<slug:login_clipper>/<fullname>",
        views.registration_form2,
        name="clipper-registration",
    ),
    path(
        "user/<username>",
        views.registration_form2,
        name="user-registration",
    ),
    path(
        "empty",
        views.registration_form2,
        name="empty-registration",
    ),
    # Autocompletion
    path(
        "autocomplete",
        views.RegistrationAutocompleteView.as_view(),
        name="cof.registration.autocomplete",
    ),
]

urlpatterns = [
    path(
        "admin/<slug:app_label>/<slug:model_name>/csv/",
        csv_views.admin_list_export,
        {"fields": ["username"]},
    ),
    # -----
    # Misc
    # -----
    path("", views.HomeView.as_view(), name="home"),
    path("reset_comptes/", views.ResetComptes.as_view(), name="reset_comptes"),
    path(
        "user/autocomplete",
        views.UserAutocompleteView.as_view(),
        name="cof-user-autocomplete",
    ),
    path("config", views.ConfigUpdate.as_view(), name="config.edit"),
    # -----
    # Authentification
    # -----
    path(
        "cof/denied",
        TemplateView.as_view(template_name="cof-denied.html"),
        name="cof-denied",
    ),
    path("cas/login", django_cas_views.LoginView.as_view(), name="cas_login_view"),
    path("cas/logout", django_cas_views.LogoutView.as_view()),
    path(
        "outsider/login",
        views.LoginExtView.as_view(),
        name="ext_login_view",
    ),
    path(
        "outsider/logout",
        django_auth_views.LogoutView.as_view(),
        {"next_page": "home"},
    ),
    path("login", views.login, name="cof-login"),
    path("logout", views.logout, name="cof-logout"),
    path("admin/logout/", views.logout),
    # -----
    # Infos persos
    # -----
    path("profile", views.profile, name="profile"),
    path(
        "outsider/password-change",
        django_auth_views.PasswordChangeView.as_view(),
        name="password_change",
    ),
    path(
        "outsider/password-change-done",
        django_auth_views.PasswordChangeDoneView.as_view(),
        name="password_change_done",
    ),
    # -----
    # Liens utiles du COF et du BdA
    # -----
    path("utile_cof", views.utile_cof, name="utile_cof"),
    path("utile_bda", views.utile_bda, name="utile_bda"),
    path("utile_bda/bda_diff", views.liste_bdadiff, name="ml_diffbda"),
    path("utile_cof/diff_cof", views.liste_diffcof, name="ml_diffcof"),
    path(
        "utile_bda/bda_revente",
        views.liste_bdarevente,
        name="ml_bda_revente",
    ),
    # -----
    # Inscription d'un nouveau membre
    # -----
    path("registration/", include(registration_patterns)),
    # -----
    # Les exports
    # -----
    path("export/", include(export_patterns)),
    # -----
    # Les sondages
    # -----
    path("survey/", include(surveys_patterns)),
    # -----
    # Evenements
    # -----
    path("event/", include(events_patterns)),
    # -----
    # Calendrier
    # -----
    path("calendar/", include(calendar_patterns)),
    # -----
    # Clubs
    # -----
    path("clubs/", include(clubs_patterns)),
]
