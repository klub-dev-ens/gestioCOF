from django.urls import path

from events import views

app_name = "events"
urlpatterns = [
    path(
        "csv/participants/<int:event_id>",
        views.participants_csv,
        name="csv-participants",
    )
]
