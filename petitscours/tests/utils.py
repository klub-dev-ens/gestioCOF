from gestioncof.tests.utils import create_user
from petitscours.models import (
    PetitCoursAbility,
    PetitCoursAttributionCounter,
    PetitCoursDemande,
    PetitCoursSubject,
)


def create_petitcours_ability(**kwargs):
    if "user" not in kwargs:
        kwargs["user"] = create_user("toto")
    if "matiere" not in kwargs:
        kwargs["matiere"] = create_petitcours_subject()
    if "niveau" not in kwargs:
        kwargs["niveau"] = "college"
    ability = PetitCoursAbility.objects.create(**kwargs)
    PetitCoursAttributionCounter.get_uptodate(ability.user, ability.matiere)
    return ability


def create_petitcours_demande(**kwargs):
    return PetitCoursDemande.objects.create(**kwargs)


def create_petitcours_subject(**kwargs):
    return PetitCoursSubject.objects.create(**kwargs)
