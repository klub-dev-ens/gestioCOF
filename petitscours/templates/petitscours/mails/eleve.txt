Salut,

Le COF a reçu une demande de petit cours qui te correspond. Tu es en haut de la liste d'attente donc on a transmis tes coordonnées, ainsi que celles de 2 autres qui correspondaient aussi (c'est la vie, on donne les numéros 3 par 3 pour que ce soit plus souple). Voici quelques infos sur l'annonce en question :

¤ Nom : {{ demande.name }}

¤ Période : {{ demande.quand }}

¤ Fréquence : {{ demande.freq }}

¤ Lieu (si préféré) : {{ demande.lieu }}

{% if matieres|length > 1 %}¤ Matières :
{% for matiere in matieres %}        ¤ {{ matiere }}
{% endfor %}{% else %}¤ Matière : {% for matiere in matieres %}{{ matiere }}
{% endfor %}{% endif %}
¤ Niveau : {{ demande.get_niveau_display }}

¤ Remarques diverses (désolé pour les balises HTML) : {{ demande.remarques }}

Voilà, cette personne te contactera peut-être sous peu, tu pourras voir les détails directement avec elle (prix, modalités, ...). Pour indication, 30 Euro/h semble être la moyenne.

Si tu te rends compte qu'en fait tu ne peux pas/plus donner de cours en ce moment, ça serait cool que tu décoches la case "Recevoir des propositions de petits cours" sur GestioCOF. Ensuite dès que tu voudras réapparaître tu pourras recocher la case et tu seras à nouveau sur la liste.

À bientôt,

-- 
Le COF, pour les petits cours