from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.forms.models import inlineformset_factory
from django.utils.translation import gettext_lazy as _
from hcaptcha.fields import hCaptchaField

from petitscours.models import PetitCoursAbility, PetitCoursDemande


class hCaptchaFieldWithErrors(hCaptchaField):
    """
    Pour l'instant, hCaptchaField ne supporte pas le paramètre `error_messages` lors de
    l'initialisation. Du coup, on les redéfinit à la main.
    """

    default_error_messages = {
        "required": _("Veuillez vérifier que vous êtes bien humain·e."),
        "error_hcaptcha": _("Erreur lors de la vérification."),
        "invalid_hcaptcha": _("Échec de la vérification !"),
    }


class DemandeForm(ModelForm):
    captcha = hCaptchaFieldWithErrors()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["matieres"].help_text = ""

    class Meta:
        model = PetitCoursDemande
        fields = (
            "name",
            "email",
            "phone",
            "quand",
            "freq",
            "lieu",
            "matieres",
            "agrege_requis",
            "niveau",
            "remarques",
        )
        widgets = {"matieres": forms.CheckboxSelectMultiple}


MatieresFormSet = inlineformset_factory(
    User,
    PetitCoursAbility,
    fields=("matiere", "niveau", "agrege"),
)
