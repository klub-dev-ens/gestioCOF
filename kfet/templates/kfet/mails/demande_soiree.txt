Bonjour,

J'aimerais organiser une soirée le {{ date }}, au thème « {{ theme|safe }} », en K-Fêt.
Elle se terminerait à {{ horaire_fin }}, et le service serait en mode {{ service }}.

Les 4 responsables de la soirée seraient :
- {{ respo1 }}
- {{ respo2 }}
- {{ respo3 }}
- {{ respo4 }}

Quelques remarques supplémentaires :
{{ remarques|safe }}

Bien cordialement,
{{ nom|safe }}
