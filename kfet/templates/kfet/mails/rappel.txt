Bonjour {{ account.first_name }},

Nous te rappelons que tu es en négatif de {{ neg_amount }}€ depuis le {{ start_date }}.
N'oublie pas de régulariser ta situation au plus vite.

En espérant te revoir très bientôt,
--
L'équipe K-Fêt
