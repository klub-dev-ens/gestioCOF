from django import forms
from django.contrib.auth.models import Group

from .models import KFetPermission


class KFetPermissionsField(forms.ModelMultipleChoiceField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("queryset", KFetPermission.objects.all())
        kwargs.setdefault("widget", forms.CheckboxSelectMultiple)
        super().__init__(*args, **kwargs)

    def label_from_instance(self, obj):
        return obj.name


class KFetGroupsField(forms.ModelMultipleChoiceField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("queryset", Group.objects.filter(kfetgroup__isnull=False))
        kwargs.setdefault("widget", forms.SelectMultiple)
        super().__init__(*args, **kwargs)

    def label_from_instance(self, obj):
        return obj.name
