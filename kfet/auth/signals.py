from django.contrib import messages
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _

from .utils import get_kfet_generic_user


@receiver(user_logged_in)
def suggest_auth_generic(sender, request, user, **kwargs):
    """
    Suggest logged in user to continue as the kfet generic user.

    Message is only added if the following conditions are met:
    - the next page (where user is going to be redirected due to successful
        authentication) is related to kfet, i.e. 'k-fet' is in its url.
    - logged in user is a kfet staff member (except the generic user).
    """
    # Filter against the next page.
    if not (hasattr(request, "GET") and "next" in request.GET):
        return

    next_page = request.GET["next"]
    generic_url = reverse("kfet.login.generic")

    if not ("k-fet" in next_page and not next_page.startswith(generic_url)):
        return

    # Filter against the logged in user.
    if not (user.has_perm("kfet.is_team") and user != get_kfet_generic_user()):
        return

    # Seems legit to add message.
    text = _("K-Fêt — Ouvrir une session partagée ?")
    messages.info(
        request,
        mark_safe(
            '<a href="#" data-url="{}" onclick="submit_url(this)">{}</a>'.format(
                generic_url, text
            )
        ),
    )
