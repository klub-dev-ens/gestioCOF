from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from shared.forms import ProtectedModelForm

from .fields import KFetGroupsField, KFetPermissionsField
from .models import KFetGroup


class GroupForm(ProtectedModelForm):
    permissions = KFetPermissionsField()

    protected_fields = ["permissions"]

    class Meta:
        model = KFetGroup
        fields = ["name", "permissions"]


class UserGroupForm(ProtectedModelForm):
    groups = KFetGroupsField(
        label=_("Statut équipe"),
        required=False,
    )

    protected_fields = ["groups"]

    class Meta:
        model = User
        fields = ["groups"]
