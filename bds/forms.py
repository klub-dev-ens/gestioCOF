from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _

from bds.models import BDSProfile

User = get_user_model()


class UserForm(forms.ModelForm):
    is_buro = forms.BooleanField(label=_("Membre du Burô"), required=False)

    class Meta:
        model = User
        fields = ["email", "first_name", "last_name"]


class UserFromClipperForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"].disabled = True

    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name"]


class UserFromScratchForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name"]


class ProfileForm(forms.ModelForm):
    class Meta:
        model = BDSProfile
        exclude = ["user"]
        widgets = {
            "birthdate": forms.DateInput(attrs={"type": "date"}, format="%Y-%m-%d")
        }
