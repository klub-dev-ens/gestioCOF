"""
Settings utilisés lors d'un développement en local (dans un virtualenv).
Active toutes les applications (de GestioCOF et de GestioBDS).

Surcharge les settings définis dans common.py
"""
import os

from . import bds_prod
from .cof_prod import *  # NOQA
from .cof_prod import BASE_DIR, INSTALLED_APPS, MIDDLEWARE, TESTING

# ---
# Merge COF and BDS configs
# ---

for app in bds_prod.INSTALLED_APPS:
    if app not in INSTALLED_APPS:
        INSTALLED_APPS.append(app)

# ---
# Tweaks for debug/local development
# ---

ALLOWED_HOSTS = []

DEBUG = True
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

if TESTING:
    PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

STATIC_URL = "/static/"
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

# Use the default cache backend for local development
CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}}

# Use the default in memory asgi backend for local development
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgiref.inmemory.ChannelLayer",
        "ROUTING": "gestioasso.routing.routing",
    }
}


# ---
# Debug tool bar
# ---


def show_toolbar(request):
    """
    On active la debug-toolbar en mode développement local sauf :
    - dans l'admin où ça ne sert pas à grand chose;
    - si la variable d'environnement DJANGO_NO_DDT est à 1 → ça permet de la désactiver
      sans modifier ce fichier en exécutant `export DJANGO_NO_DDT=1` dans le terminal
      qui lance `./manage.py runserver`.

    Autre side effect de cette fonction : on ne fait pas la vérification de INTERNAL_IPS
    que ferait la debug-toolbar par défaut, ce qui la fait fonctionner aussi à
    l'intérieur de Vagrant (comportement non testé depuis un moment…)
    """
    env_no_ddt = bool(os.environ.get("DJANGO_NO_DDT", None))
    return DEBUG and not env_no_ddt and not request.path.startswith("/admin/")


if not TESTING:
    INSTALLED_APPS += ["debug_toolbar"]
    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE
    DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": show_toolbar}
