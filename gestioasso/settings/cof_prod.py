"""
Settings de production de GestioCOF.

Surcharge les settings définis dans common.py
"""

import os
from datetime import timedelta

from django.utils import timezone

from .common import *  # NOQA
from .common import (
    AUTHENTICATION_BACKENDS,
    BASE_DIR,
    INSTALLED_APPS,
    MIDDLEWARE,
    TEMPLATES,
    import_secret,
)

# ---
# COF-specific secrets
# ---

REDIS_PASSWD = import_secret("REDIS_PASSWD")
REDIS_DB = import_secret("REDIS_DB")
REDIS_HOST = import_secret("REDIS_HOST")
REDIS_PORT = import_secret("REDIS_PORT")

HCAPTCHA_SITEKEY = import_secret("HCAPTCHA_SITEKEY")
HCAPTCHA_SECRET = import_secret("HCAPTCHA_SECRET")
KFETOPEN_TOKEN = import_secret("KFETOPEN_TOKEN")

# ---
# COF-only Django settings
# ---

ALLOWED_HOSTS = ["cof.ens.fr", "www.cof.ens.fr", "dev.cof.ens.fr"]

INSTALLED_APPS = (
    [
        "gestioncof",
        # Must be before django admin
        # https://github.com/infoportugal/wagtail-modeltranslation/issues/193
        "wagtail_modeltranslation",
        "wagtail_modeltranslation.makemigrations",
        "wagtail_modeltranslation.migrate",
        "modeltranslation",
    ]
    + INSTALLED_APPS
    + [
        "bda",
        "petitscours",
        "hcaptcha",
        "kfet",
        "kfet.open",
        "channels",
        "djconfig",
        "wagtail.contrib.forms",
        "wagtail.contrib.redirects",
        "wagtail.embeds",
        "wagtail.sites",
        "wagtail.users",
        "wagtail.snippets",
        "wagtail.documents",
        "wagtail.images",
        "wagtail.search",
        "wagtail.admin",
        "wagtail.core",
        "wagtail.contrib.modeladmin",
        "wagtail.contrib.routable_page",
        "wagtailmenus",
        "modelcluster",
        "taggit",
        "kfet.auth",
        "kfet.cms",
        "gestioncof.cms",
        "django_js_reverse",
    ]
)

MIDDLEWARE = (
    ["corsheaders.middleware.CorsMiddleware"]
    + MIDDLEWARE
    + [
        "djconfig.middleware.DjConfigMiddleware",
        "wagtail.core.middleware.SiteMiddleware",
        "wagtail.contrib.redirects.middleware.RedirectMiddleware",
    ]
)

TEMPLATES[0]["OPTIONS"]["context_processors"] += [
    "wagtailmenus.context_processors.wagtailmenus",
    "djconfig.context_processors.config",
    "gestioncof.shared.context_processor",
    "kfet.auth.context_processors.temporary_auth",
    "kfet.context_processors.config",
]

STATIC_ROOT = os.path.join(
    os.path.dirname(os.path.dirname(BASE_DIR)), "public", "gestion", "static"
)

STATIC_URL = "/gestion/static/"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
MEDIA_URL = "/gestion/media/"

CORS_ORIGIN_WHITELIST = ("bda.ens.fr", "www.bda.ens.fr" "cof.ens.fr", "www.cof.ens.fr")


# ---
# Auth-related stuff
# ---

AUTHENTICATION_BACKENDS = (
    [
        # Must be in first
        "kfet.auth.backends.BlockFrozenAccountBackend"
    ]
    + AUTHENTICATION_BACKENDS
    + [
        "gestioncof.shared.COFCASBackend",
        "kfet.auth.backends.GenericBackend",
    ]
)
LOGIN_URL = "cof-login"
LOGIN_REDIRECT_URL = "home"

# ---
# Cache settings
# ---

CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": "redis://:{passwd}@{host}:{port}/{db}".format(
            passwd=REDIS_PASSWD, host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB
        ),
    }
}


# ---
# Channels settings
# ---

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [
                (
                    "redis://:{passwd}@{host}:{port}/{db}".format(
                        passwd=REDIS_PASSWD,
                        host=REDIS_HOST,
                        port=REDIS_PORT,
                        db=REDIS_DB,
                    )
                )
            ]
        },
        "ROUTING": "gestioasso.routing.routing",
    }
}


# ---
# reCAPTCHA settings
# https://github.com/praekelt/django-recaptcha
#
# Default settings authorize reCAPTCHA usage for local developement.
# Public and private keys are appended in the 'prod' module settings.
# ---

NOCAPTCHA = True
RECAPTCHA_USE_SSL = True


# ---
# Wagtail settings
# ---

WAGTAIL_SITE_NAME = "GestioCOF"
WAGTAIL_ENABLE_UPDATE_CHECK = False
TAGGIT_CASE_INSENSITIVE = True


# ---
# Django-js-reverse settings
# ---

JS_REVERSE_JS_VAR_NAME = "django_urls"
# Quand on aura namespace les urls...
# JS_REVERSE_INCLUDE_ONLY_NAMESPACES = ['k-fet']


# ---
# Mail config
# ---

MAIL_DATA = {
    "petits_cours": {
        "FROM": "Le COF <cof@ens.fr>",
        "BCC": "archivescof@gmail.com",
        "REPLYTO": "cof@ens.fr",
    },
    "rappels": {"FROM": "Le BdA <bda@ens.fr>", "REPLYTO": "Le BdA <bda@ens.fr>"},
    "rappel_negatif": {
        "FROM": "La K-Fêt <chefs-k-fet@ens.fr>",
        "REPLYTO": "La K-Fêt <chefs-k-fet@ens.fr>",
    },
    "revente": {
        "FROM": "BdA-Revente <bda-revente@ens.fr>",
        "REPLYTO": "BdA-Revente <bda-revente@ens.fr>",
    },
}

# ---
# kfet history limits
# ---

# L'historique n'est accesible que d'aujourd'hui
# à aujourd'hui - KFET_HISTORY_DATE_LIMIT
KFET_HISTORY_DATE_LIMIT = timedelta(days=7)

# Limite plus longue pour les chefs/trez
# (qui ont la permission kfet.access_old_history)
KFET_HISTORY_LONG_DATE_LIMIT = timedelta(days=30)

# These accounts don't represent actual people and can be freely accessed
# Identification based on trigrammes
KFET_HISTORY_NO_DATE_LIMIT_TRIGRAMMES = ["LIQ", "#13"]
KFET_HISTORY_NO_DATE_LIMIT = timezone.datetime(1794, 10, 30)  # AKA the distant past
