"""
Settings de production de GestioBDS.

Surcharge les settings définis dans common.py
"""

from .common import *  # NOQA
from .common import INSTALLED_APPS

# ---
# BDS-only Django settings
# ---

ALLOWED_HOSTS = ["bds.ens.fr", "www.bds.ens.fr", "dev.cof.ens.fr"]

INSTALLED_APPS += ["bds", "events", "clubs", "authens"]

STATIC_ROOT = "/srv/bds.ens.fr/public/gestion2/static"
STATIC_URL = "/gestion2/static/"
MEDIA_ROOT = "/srv/bds.ens.fr/gestion2/media"
MEDIA_URL = "/gestion2/media/"


# ---
# Auth-related stuff
# ---

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "authens.backends.ENSCASBackend",
    "authens.backends.OldCASBackend",
]

AUTHENS_USE_OLDCAS = False

LOGIN_URL = "authens:login"
LOGIN_REDIRECT_URL = "bds:home"
LOGOUT_REDIRECT_URL = "bds:home"
