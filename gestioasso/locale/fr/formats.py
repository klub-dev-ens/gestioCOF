"""
Formats français.
"""

DATETIME_FORMAT = r"l j F Y \à H\hi"
DATE_FORMAT = r"l j F Y"
TIME_FORMAT = r"H\hi"
